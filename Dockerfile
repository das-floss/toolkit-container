FROM debian:bookworm-slim

RUN apt-get update && apt-get install -y \
    bind9-host \
    iputils-ping \
    inetutils-telnet \
    default-mysql-client \
    curl \
    wget \
    jq \
    git \
  && rm -rf /var/lib/apt/lists/*
